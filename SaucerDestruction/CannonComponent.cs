﻿using NuroseMobile.Graphics.Components;
using NuroseMobile.System;
using NuroseMobile.System.Components;
using NuroseMobile.Types;

namespace SaucerDestruction
{
    public class CannonComponent : Component
    {
        public float Damage = 10;
        float t = 0;
        private void Start()
        {
            var s = Entity.AddComponent<AnimatedSpriteRenderer>();
            Transform.Origin = Vector2f.One / 2f;
            s.Frames.AddRange(new[] { "cannon0.png", "cannon1.png" });
            s.Framerate = 16;
            s.Loop = true;
            s.Animated = true;
            Transform.LocalSize = Vector2f.One * 24;
        }

        private void Update()
        {
            t += (float)Time.DeltaTime;
            if (GameController.Main.Player.Transform.Bounds.ContainsPoint(Transform.LocalPosition) || t > 7 || Transform.LocalPosition.Y > 448)
            {
                if (GameController.Main.Player.Transform.Bounds.ContainsPoint(Transform.LocalPosition))
                {
                    GameController.Main.SpawnExplosion(Transform.LocalPosition);
                    GameController.Main.Player.Damage(Damage);
                }
                Entity.Destroy();
            }
        }

    }
}
