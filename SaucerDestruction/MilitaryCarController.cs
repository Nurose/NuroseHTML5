﻿using NuroseMobile.System;
using NuroseMobile.System.Components;
using NuroseMobile.Types;
using NuroseMobile.Utilities;
using System;

namespace SaucerDestruction
{
    public class MilitaryCarController : CarController
    {
        private float maxDistance = Mathf.RandomFloat(15, 300);
        protected override void Drive()
        {
            renderer.Depth = -0.4f;
            float delta = player.Transform.LocalPosition.X - Transform.LocalPosition.X;
            if (!(onGround && Health > 30 && Mathf.Abs(Transform.Angle) < 10)) return;//if alive

            if (Mathf.Abs(delta) > maxDistance)
            {
                PhysicsComponent.Velocity += Vector2f.Right * (((Flipped ? -1 : 1) * Speed) * 2.5f * (delta > 0 ? 1 : -1) - PhysicsComponent.Velocity.X) * Time.GetTimeIndependencyMultiplier() * .5f;
            }
            else
            {
                PhysicsComponent.Velocity = new Vector2f(PhysicsComponent.Velocity.X * 0.9f, PhysicsComponent.Velocity.Y);
            }

            if (Math.Abs(delta) < 301)
                if (Mathf.RandomFloat() > 0.97f)
                    Shoot();
        }

        private void Shoot()
        {
            Entity bullet = new Entity("b", Transform.LocalPosition);
            var b = bullet.AddComponent<BulletComponent>();
            b.Speed = 10;
            var dir = (player.Transform.LocalPosition - Transform.LocalPosition);
            bullet.Transform.Angle = (float)Math.Atan2(dir.Y, dir.X) / (float)Mathf.DegreeToRadian;
            Entity.Scene.AddEntity(bullet);
        }
    }
}