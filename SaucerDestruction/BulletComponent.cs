﻿using NuroseMobile.Audio;
using NuroseMobile.Graphics.Components;
using NuroseMobile.System;
using NuroseMobile.System.Components;
using NuroseMobile.Types;
using NuroseMobile.Utilities;

namespace SaucerDestruction
{
    public class BulletComponent : Component
    {
        public float Speed = 6;
        public float Damage = 1;
        float t = 0;
        private void Start()
        {
            var s = Entity.AddComponent<SpriteRenderer>();
            Transform.Origin = Vector2f.One / 2f;
            s.ImageSource = "bullet.png";
            Transform.LocalSize = Vector2f.One * 7;
            AudioPlayer.Get("shot.wav").Play();
        }

        private void Update()
        {
            t += (float)Time.DeltaTime;
            Transform.LocalPosition += new Vector2f(Mathf.Cos(Transform.Angle), Mathf.Sin(Transform.Angle)) * Speed;
            if (GameController.Main.Player.Transform.Bounds.ContainsPoint(Transform.LocalPosition) || t > 3)
            {
                if (GameController.Main.Player.Transform.Bounds.ContainsPoint(Transform.LocalPosition))
                    GameController.Main.Player.Damage(Damage);
                Entity.Destroy();
            }
        }
    }
}
