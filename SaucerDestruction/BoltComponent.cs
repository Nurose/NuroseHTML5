﻿using NuroseMobile.Graphics.Components;
using NuroseMobile.System;
using NuroseMobile.System.Components;
using NuroseMobile.Types;
using NuroseMobile.Utilities;

namespace SaucerDestruction
{
    public class BoltComponent : Component
    {
        private void Start()
        {
            var s = Entity.AddComponent<SpriteRenderer>();
            Transform.Origin = Vector2f.One / 2f;
            s.ImageSource = "bolt.png";
            Transform.LocalSize = Vector2f.One * 25;
            GameController.Main.AddScreenShake(4);
        }

        private void Update()
        {
            Transform.LocalPosition -= new Vector2f(Mathf.Cos(Transform.Angle), Mathf.Sin(Transform.Angle)) * 40;
            if (Transform.LocalPosition.Y > 448)
            {
                GameController.Main.SpawnAlienExplosion(new Vector2f(Transform.LocalPosition.X, 448));
                Entity.Destroy();
            }
        }
    }
}
