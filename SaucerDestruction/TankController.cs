﻿using NuroseMobile.Graphics;
using NuroseMobile.System;
using NuroseMobile.System.Components;
using NuroseMobile.Types;
using NuroseMobile.Utilities;
using System;

namespace SaucerDestruction
{
    public class TankController : CarController
    {
        private float maxDistance = Mathf.RandomFloat(150, 300);
        protected override void Drive()
        {
            renderer.Depth = -0.4f;
            float delta = player.Transform.LocalPosition.X - Transform.LocalPosition.X;
            if (!(onGround && Health > 30 && Mathf.Abs(Transform.Angle) < 10)) return;//if alive

            if (Mathf.Abs(delta) > maxDistance)
            {
                PhysicsComponent.Velocity += Vector2f.Right * ((Speed) * 2.5f * (delta > 0 ? 1 : -1) - PhysicsComponent.Velocity.X) * Time.GetTimeIndependencyMultiplier() * .5f;
            }
            else
            {
                PhysicsComponent.Velocity = new Vector2f(PhysicsComponent.Velocity.X * 0.9f, PhysicsComponent.Velocity.Y);
            }

            if (Math.Abs(delta) < 501)
                if (Mathf.RandomFloat() > 0.992f)
                    Shoot();
        }

        private void Shoot()
        {
            Entity bullet = new Entity("b", Transform.LocalPosition);
            bullet.AddComponent<CannonComponent>();
            var b = bullet.AddComponent<PhysicsComponent>();
            b.Dampening = 1;
            b.UseGravity = true;
            b.Gravity *= 0.1f;
            b.Velocity = (player.Transform.LocalPosition - Transform.LocalPosition) * 0.03f;
            Entity.Scene.AddEntity(bullet);
        }

        private void Render()
        {
            var a = (player.Transform.LocalPosition - Transform.LocalPosition);
            Renderer.DrawImage(ImageResourceManager.Load("tankBarrel.png"), Transform.LocalPosition + Vector2f.FromAngle(Transform.Angle - 90, 15) + Vector2f.FromAngle(Transform.Angle, Flipped ? -18 : 18), new Vector2f(54, 5), (float)Math.Atan2(a.Y, a.X) / (float)Mathf.DegreeToRadian, new Vector2f(0, 2.5f), 1, false, 0.38f);
        }
    }
}