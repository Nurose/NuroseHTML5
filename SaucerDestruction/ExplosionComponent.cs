﻿using NuroseMobile.Audio;
using NuroseMobile.Graphics.Components;
using NuroseMobile.System;
using NuroseMobile.System.Components;
using NuroseMobile.Types;
using NuroseMobile.Utilities;

namespace SaucerDestruction
{
    public class ExplosionComponent : Component
    {
        AnimatedSpriteRenderer renderer;
        private void Start()
        {
            renderer = GetComponent<AnimatedSpriteRenderer>();
            int v = Mathf.RandomFloat() > 0.5f ? 0 : 1;
            for (int i = 0; i < 14; i++)
                renderer.Frames.Add("ex" + v + "/explosion" + i + ".png");

            Transform.Origin = Vector2f.One / 2f;
            renderer.Framerate = 16;
            GameController.Main.AddScreenShake(Mathf.RandomFloat(8, 15));
            Transform.LocalSize = Vector2f.One * Mathf.RandomFloat(100, 150);
            Transform.Angle = Mathf.RandomFloat(0, 360);
            var everyone = Entity.Scene.GetAllComponentsOfType<PhysicsComponent>();
            foreach (var obj in everyone)
            {
                if (obj.Entity.Name == "Player") continue;
                var dir = (obj.Transform.LocalPosition - Transform.LocalPosition);
                if (dir.Magnitude > 150) continue;

                dir = dir.Normalised;
                obj.Velocity += dir * Mathf.RandomFloat(2, 5);
                obj.Velocity -= Vector2f.Up * Mathf.RandomFloat(3, 6);
                obj.RotationalVelocity += Mathf.RandomFloat(-3, 3);
            }

            AudioPlayer.Get("explosion" + ((int)Mathf.RandomInt(0, 3)).ToString() + ".wav").Play(Mathf.RandomFloat(0.1f,0.3f));
        }

        private void Update()
        {
            if (!renderer.Animated)
                Entity.Destroy();
        }
    }
}