﻿using NuroseMobile.Graphics.Components;
using NuroseMobile.System;
using NuroseMobile.System.Components;
using NuroseMobile.Types;
using NuroseMobile.Utilities;
using System;

namespace SaucerDestruction
{
    [RequireComponent(typeof(PhysicsComponent))]
    [SingleComponent]
    public class CarController : Component
    {
        public string Image { get; set; } = "";
        public string DamagedImage { get; set; } = "";
        public float Speed { get; set; }
        public bool Flipped { get; set; }

        public PhysicsComponent PhysicsComponent;
        protected PlayerController player;
        protected bool onGround;
        private float _health = 100;
        protected SpriteRenderer renderer;
        public int KillScore = 1;
        public float Health
        {
            get => _health; set
            {
                if (_health > 50 && value < 50)
                    GetComponent<SpriteRenderer>().ImageSource = DamagedImage;
                else
                    GetComponent<SpriteRenderer>().ImageSource = Image;
                _health = value;
            }
        }
        private void Start()
        {
            Speed = Mathf.RandomFloat(3f, 5f);
            PhysicsComponent = GetComponent<PhysicsComponent>();
            player = GameController.Main.Player;

            renderer = GetComponent<SpriteRenderer>();
            renderer.Depth = 0.5f;
            renderer.VerticalFlip = Flipped;
        }

        private void Update()
        {
            onGround = false;
            if (Transform.LocalPosition.Y > 448 - Transform.LocalSize.Y / 2)
            {
                if (PhysicsComponent.Velocity.Y > 5) Damage(PhysicsComponent.Velocity.Y * 7.1f);
                onGround = true;
                Transform.LocalPosition = new Vector2f(Transform.LocalPosition.X, 448 - Transform.LocalSize.Y / 2);
                PhysicsComponent.Velocity = new Vector2f(PhysicsComponent.Velocity.X, 0);
                PhysicsComponent.RotationalVelocity *= 0.9f;
                if (Health < 50)
                    Transform.Angle = Mathf.Lerp(Transform.Angle % 360, (float)Math.Round(Transform.Angle / 180f) * 180f, 0.2f);
                else
                    Transform.Angle = Mathf.Lerp(Transform.Angle % 360, 0, 0.2f);
                PhysicsComponent.Velocity *= 0.9f;
            }

            Drive();

            if (Flipped)
            {
                if (Transform.LocalPosition.X < -2048 + Transform.LocalSize.X / 2)
                    Entity.Destroy();

                if (Transform.LocalPosition.X > 2048 - Transform.LocalSize.X / 2 && Health < 100)
                    Entity.Destroy();
            }
            else
            {
                if (Transform.LocalPosition.X > 2048 + Transform.LocalSize.X / 2)
                    Entity.Destroy();

                if (Transform.LocalPosition.X < -2048 - Transform.LocalSize.X / 2 && Health < 100)
                    Entity.Destroy();
            }

            if (Transform.Bounds.Intersects(player.Transform.Bounds))
            {
                var hit = 0.1f * (player.GetComponent<PhysicsComponent>().Velocity + Vector2f.Down * player.GetComponent<PhysicsComponent>().Velocity.Magnitude * Mathf.RandomFloat(0, 3));
                PhysicsComponent.Velocity += hit;
                PhysicsComponent.RotationalVelocity += hit.X * Mathf.RandomFloat(.5f, 1);
                GameController.Main.AddScreenShake(hit.Magnitude * 1);
                if (Health > 0 && hit.Magnitude > 0.1f && Mathf.RandomFloat() > 0.95f)
                    Damage(hit.Magnitude * 1.4f);
            }

            if (Health < 20)
            {
                if (Mathf.RandomFloat() > 0.92)
                {
                    var point = Transform.LocalPosition;
                    point += new Vector2f(Mathf.Cos(Transform.Angle), Mathf.Sin(Transform.Angle)) * Mathf.RandomFloat(-0.5f, 0.5f) * Transform.LocalSize.X;
                    point += new Vector2f(Mathf.Cos(Transform.Angle + 90), Mathf.Sin(Transform.Angle + 90)) * Mathf.RandomFloat(-0.5f, 0.5f) * Transform.LocalSize.Y;
                    //point.X *= Mathf.Cos(Transform.Angle);
                    //point.Y *= Mathf.Sin(Transform.Angle);

                    Entity entity = new Entity("", point);
                    entity.AddComponent<FireComponent>();
                    Entity.Scene.AddEntity(entity);
                }
            }
        }

        public void Damage(float i)
        {
            Health -= i;

            if (Health < 0)
            {
                GameController.Main.SpawnExplosion(Transform.LocalPosition);
                GameController.Main.AddScore(KillScore);
                Entity.Destroy();
            }
        }

        protected virtual void Drive()
        {
            if (onGround && Health > 50 && Mathf.Abs(Transform.Angle) < 10)
                PhysicsComponent.Velocity += Vector2f.Right * (((Flipped ? -1 : 1) * Speed) - PhysicsComponent.Velocity.X) * Time.GetTimeIndependencyMultiplier() * .5f;
        }
    }
}