﻿using NuroseMobile.Graphics;
using NuroseMobile.Graphics.Components;
using NuroseMobile.System;
using NuroseMobile.System.Components;
using NuroseMobile.Types;

namespace SaucerDestruction
{
    public class Program
    {
        public static void Main()
        {
            ImageResourceManager.Preload(
                "bolt.png",
                "ex0/explosion0.png",
                "ex0/explosion1.png",
                "ex0/explosion2.png",
                "ex0/explosion3.png",
                "ex0/explosion4.png",
                "ex0/explosion5.png",
                "ex0/explosion6.png",
                "ex0/explosion7.png",
                "ex0/explosion8.png",
                "ex0/explosion9.png",
                "ex0/explosion10.png",
                "ex0/explosion11.png",
                "ex0/explosion12.png",
                "ex0/explosion13.png",
                "ex1/explosion0.png",
                "ex1/explosion1.png",
                "ex1/explosion2.png",
                "ex1/explosion3.png",
                "ex1/explosion4.png",
                "ex1/explosion5.png",
                "ex1/explosion6.png",
                "ex1/explosion7.png",
                "ex1/explosion8.png",
                "ex1/explosion9.png",
                "ex1/explosion10.png",
                "ex1/explosion11.png",
                "ex1/explosion12.png",
                "ex1/explosion13.png",

                "aex0/explosion0.png",
                "aex0/explosion1.png",
                "aex0/explosion2.png",
                "aex0/explosion3.png",
                "aex0/explosion4.png",
                "aex0/explosion5.png",
                "aex0/explosion6.png",
                "aex0/explosion7.png",
                "aex0/explosion8.png",
                "aex0/explosion9.png",
                "aex0/explosion10.png",
                "aex0/explosion11.png",
                "aex0/explosion12.png",
                "aex0/explosion13.png",
                "aex0/explosion14.png",
                "aex0/explosion15.png",
                "aex0/explosion16.png",
                "aex1/explosion0.png",
                "aex1/explosion1.png",
                "aex1/explosion2.png",
                "aex1/explosion3.png",
                "aex1/explosion4.png",
                "aex1/explosion5.png",
                "aex1/explosion6.png",
                "aex1/explosion7.png",
                "aex1/explosion8.png",
                "aex1/explosion9.png",
                "aex1/explosion10.png",
                "aex1/explosion11.png",
                "aex1/explosion12.png",
                "aex1/explosion13.png",
                "aex1/explosion14.png",
                "aex1/explosion15.png",
                "aex1/explosion16.png"
                );
            SaucerDestructionGame saucerDestructionGame = new SaucerDestructionGame();
        }
    }

    public class SaucerDestructionGame
    {
        private static SaucerDestructionGame main;
        private Game game;
        private Scene menuScene;
        private Scene mainScene;
        private Scene gameOverScene;

        public SaucerDestructionGame()
        {
            main = this;
            game = new Game(new Vector2i(780, 560));

            menuScene = game.CreateScene("Menu");
            mainScene = game.CreateScene("Game");
            gameOverScene = game.CreateScene("Game Over");

            SetupMenuScene();
            SetupGameScene();
            SetupGameOverScene();

            game.ActiveScene = menuScene;

            game.Start();
        }

        private void SetupMenuScene()
        {
            menuScene.BackgroundColor = Color.Black;
            menuScene.BackgroundImage = "menuBackground.png";

            //Play button
            Entity playButton = new Entity("Play Button", new Vector2f(146, 366));
            playButton.AddComponent<SpriteRenderer>().ImageSource = "play.png";
            playButton.Transform.LocalSize = new Vector2f(87, 42);
            playButton.AddComponent<ButtonComponent>().ClickAction = delegate { game.ActiveScene = mainScene; };
            menuScene.AddEntity(playButton);
        }

        internal static void GameOver()
        {
            main.game.ActiveScene = main.gameOverScene;
        }

        private void SetupGameScene()
        {
            mainScene.BackgroundColor = Color.Black;
            mainScene.BackgroundImage = "gameBackground.png";

            //Player
            Entity player = new Entity("Player", Renderer.Resolution / 2);
            player.Transform.LocalSize = new Vector2f(138, 34);
            player.AddComponent<SpriteRenderer>().ImageSource = "ufo.png";
            player.AddComponent<PhysicsComponent>();
            player.AddComponent<PlayerController>();
            mainScene.AddEntity(player);

            //Ground
            Entity ground = new Entity("Ground", new Vector2f(-2048, 448));
            ground.Transform.LocalSize = new Vector2f(4096, 200);
            ground.AddComponent<SpriteRenderer>().ImageSource = "ground.png";
            ground.GetComponent<SpriteRenderer>().Depth = -0.1f;
            mainScene.AddEntity(ground);

            //Game controller
            Entity gameController = new Entity("Game Controller");
            gameController.AddComponent<GameController>().ToFollow = player.Transform;
            mainScene.AddEntity(gameController);
        }

        private void SetupGameOverScene()
        {
            gameOverScene.BackgroundImage = "gameover.png";
        }
    }
}