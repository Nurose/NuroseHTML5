﻿using NuroseMobile.Graphics.Components;
using NuroseMobile.System;
using NuroseMobile.System.Components;
using NuroseMobile.Types;
using NuroseMobile.Utilities;

namespace SaucerDestruction
{
    public class FireComponent : Component
    {
        AnimatedSpriteRenderer renderer;
        private void Start()
        {
            renderer = Entity.AddComponent<AnimatedSpriteRenderer>();
            int v = Mathf.RandomFloat() > 0.5f ? 0 : 1;
            for (int i = 0; i < 16; i++)
                renderer.Frames.Add("fire" + v + "/fire" + i + ".png");

            Transform.Origin = Vector2f.One / 2f;
            renderer.Framerate = 16;
            Transform.LocalSize = Vector2f.One * Mathf.RandomFloat(45, 55);
            Transform.Angle = Mathf.RandomFloat(-5, 5);
            //  AudioPlayer.Play("in.wav");
        }

        private void Update()
        {
            if (!renderer.Animated)
                Entity.Destroy();
        }
    }
}