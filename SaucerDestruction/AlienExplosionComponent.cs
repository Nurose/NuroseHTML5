﻿using NuroseMobile.Audio;
using NuroseMobile.Graphics.Components;
using NuroseMobile.System;
using NuroseMobile.System.Components;
using NuroseMobile.Types;
using NuroseMobile.Utilities;

namespace SaucerDestruction
{
    public class AlienExplosionComponent : Component
    {
        AnimatedSpriteRenderer renderer;
        private void Start()
        {
            renderer = GetComponent<AnimatedSpriteRenderer>();
            int v = Mathf.RandomFloat() > 0.5f ? 0 : 1;
            for (int i = 0; i < 17; i++)
                renderer.Frames.Add("aex" + v + "/explosion" + i + ".png");

            Transform.Origin = Vector2f.One / 2f;
            renderer.Framerate = 16;
            GameController.Main.AddScreenShake(Mathf.RandomFloat(9, 25));
            Transform.LocalSize = Vector2f.One * Mathf.RandomFloat(140, 150);
            Transform.Angle = Mathf.RandomFloat(0, 360);
            //  AudioPlayer.Play("in.wav");

            var everyone = Entity.Scene.GetAllComponentsOfType<PhysicsComponent>();
            foreach (var obj in everyone)
            {
                if (obj.Entity.Name == "Player") continue;
                var dir = (obj.Transform.LocalPosition - Transform.LocalPosition);
                if (dir.Magnitude > 180) continue;

                dir = dir.Normalised;
                obj.Velocity += dir * Mathf.RandomFloat(4, 7);
                obj.Velocity -= Vector2f.Up * Mathf.RandomFloat(5, 8);
                obj.RotationalVelocity += Mathf.RandomFloat(-5, 5);

                var c = obj.GetComponent<CarController>();
                if (c != null)
                    c.Damage(15f);
            }

            AudioPlayer.Get("explosion" + ((int)Mathf.RandomInt(0, 3)).ToString() + ".wav").Play(Mathf.RandomFloat(0.1f, 0.3f));
        }

        private void Update()
        {
            if (!renderer.Animated)
                Entity.Destroy();
        }
    }
}