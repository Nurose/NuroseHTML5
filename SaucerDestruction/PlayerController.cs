﻿using NuroseMobile.Graphics.Components;
using NuroseMobile.System;
using NuroseMobile.System.Components;
using NuroseMobile.Types;
using NuroseMobile.Utilities;
using System.Linq;

namespace SaucerDestruction
{
    [RequireComponent(typeof(PhysicsComponent))]
    [SingleComponent]
    public class PlayerController : Component
    {
        public float Speed { get; set; } = 1;
        public float Health
        {
            get => _health;
            set
            {
                _health = value;
                healthLabel.Text = (int)_health + "% hull";
                //healthSlider.Transform.LocalSize = new Vector2f(Mathf.Clamp(_health, 0, 100) * 2.6f, 8);
            }
        }
        private PhysicsComponent physicsComponent;
        private SpriteRenderer weaponLabel;
        private TextRenderer healthLabel;
        private RectangleRenderer healthSlider;
        public Weapon EquippedWeapon
        {
            get
            {
                return weapons[EquippedWeaponIndex];
            }
            set
            {
                EquippedWeapon.EndUse();
                EquippedWeapon.IsUsing = false;
                EquippedWeaponIndex = weapons.ToList().IndexOf(value);
                if (Input.IsKeyHeld(Key.SPACE))
                {
                    EquippedWeapon.IsUsing = true;
                    EquippedWeapon.StartUse();
                }
            }
        }

        public int EquippedWeaponIndex { get; set; } = 0;

        private Weapon[] weapons;
        private float _health;

        private void Start()
        {
            Transform.Origin = Vector2f.One / 2f;
            GetComponent<SpriteRenderer>().Depth = -1;
            physicsComponent = GetComponent<PhysicsComponent>();
            physicsComponent.UseGravity = false;
            physicsComponent.Dampening = 0.9f;

            weapons = new Weapon[] {
                Entity.AddComponent<TractorBeam>(),
                Entity.AddComponent<LaserBeam>(),
                Entity.AddComponent<BoltWeapon>()
            };

            Entity weaponlabelentity = new Entity("", Vector2f.One * 5);
            weaponLabel = weaponlabelentity.AddComponent<SpriteRenderer>();
            weaponLabel.Depth = float.NegativeInfinity;
            weaponLabel.IgnoreCameraPosition = true;
            weaponlabelentity.Transform.LocalSize = Vector2f.One * 64;
            Entity.Scene.AddEntity(weaponlabelentity);
            EquippedWeaponIndex = 0;

            //CreateHealthDisplay();
            CreateHealthLabel();
            Health = 100;
        }

        private void CreateHealthLabel()
        {
            Entity healthDisplay = new Entity("", new Vector2f(8, 64 + 10 + 16 + 16 + 5));
            healthDisplay.Transform.LocalSize = new Vector2f(256, 64);
            healthLabel = healthDisplay.AddComponent<TextRenderer>();
            healthLabel.FontStyle = "16px Roboto";
            healthLabel.Depth = float.NegativeInfinity;
            healthLabel.IgnoreCameraPosition = true;
            healthLabel.Color = Color.White;
            Entity.Scene.AddEntity(healthDisplay);
        }

        private void CreateHealthDisplay()
        {
            Entity healthbar = new Entity("", new Vector2f(74, 5));
            healthbar.Transform.LocalSize = Vector2f.One * 8;
            healthSlider = healthbar.AddComponent<RectangleRenderer>();
            healthSlider.IgnoreCameraPosition = true;
            Entity.Scene.AddEntity(healthbar);
        }

        public void Damage(float damage)
        {
            Health -= damage;
            physicsComponent.Velocity += 2 * new Vector2f(Mathf.RandomFloat(-.5f, .5f), Mathf.RandomFloat(-.5f, .5f));
            GameController.Main.AddScreenShake(.2f);
            if (Health < 0)
                SaucerDestructionGame.GameOver();
        }

        private void Update()
        {
            Transform.Angle = physicsComponent.Velocity.X * .9f;
            Transform.LocalPosition = new Vector2f(Mathf.Clamp(Transform.LocalPosition.X, -1960, 1960), Mathf.Clamp(Transform.LocalPosition.Y, 16, 414));
            Movement();
            Abilities();
        }

        private void Abilities()
        {
            if (Input.IsKeyPressed(Key.KEY_E))
            {
                EquippedWeapon.EndUse();
                EquippedWeapon.IsUsing = false;
                EquippedWeaponIndex++;
                if (EquippedWeaponIndex < 0) EquippedWeaponIndex = weapons.Length - 1;
                if (EquippedWeaponIndex >= weapons.Length) EquippedWeaponIndex = 0;
                if (Input.IsKeyHeld(Key.SPACE))
                {
                    EquippedWeapon.IsUsing = true;
                    EquippedWeapon.StartUse();
                }
            }

            if (Input.IsKeyPressed(Key.KEY_Q))
            {
                EquippedWeapon.EndUse();
                EquippedWeapon.IsUsing = false;
                EquippedWeaponIndex--;
                if (EquippedWeaponIndex < 0) EquippedWeaponIndex = weapons.Length - 1;
                if (EquippedWeaponIndex >= weapons.Length) EquippedWeaponIndex = 0;
                if (Input.IsKeyHeld(Key.SPACE))
                {
                    EquippedWeapon.IsUsing = true;
                    EquippedWeapon.StartUse();
                }
            }

            if (Input.IsKeyPressed(Key.KEY_1))
                EquippedWeapon = weapons[0];
            if (Input.IsKeyPressed(Key.KEY_2))
                EquippedWeapon = weapons[1];
            if (Input.IsKeyPressed(Key.KEY_3))
                EquippedWeapon = weapons[2];

            weaponLabel.ImageSource = EquippedWeapon.IconPath;

            if (Input.IsKeyPressed(Key.SPACE))
            {
                EquippedWeapon.IsUsing = true;
                EquippedWeapon.StartUse();
            }

            if (EquippedWeapon.IsUsing)
                EquippedWeapon.Using();

            if (Input.IsKeyReleased(Key.SPACE))
            {
                EquippedWeapon.IsUsing = false;
                EquippedWeapon.EndUse();
            }
        }

        private void Movement()
        {
            float multiplier = Input.IsKeyHeld(Key.SHIFT) ? 1.5f : 1f;
            if (Input.IsKeyHeld(Key.KEY_D))
                physicsComponent.Velocity += Vector2f.Right * Speed * multiplier;
            if (Input.IsKeyHeld(Key.KEY_A))
                physicsComponent.Velocity -= Vector2f.Right * Speed * multiplier;
            if (Input.IsKeyHeld(Key.KEY_S))
                physicsComponent.Velocity += Vector2f.Up * Speed * multiplier * .8f;
            if (Input.IsKeyHeld(Key.KEY_W))
                physicsComponent.Velocity -= Vector2f.Up * Speed * multiplier * .8f;
        }
    }
}