﻿using NuroseMobile.Audio;
using NuroseMobile.System;
using NuroseMobile.System.Components;
using NuroseMobile.Types;
using NuroseMobile.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaucerDestruction
{
    class BoltWeapon : Weapon
    {
        private void Start()
        {
            Name = "Blaster";
            IconPath = "boltIcon.png";
        }

        public override void EndUse()
        {
        }

        public override void StartUse()
        {
            GetComponent<PhysicsComponent>().Velocity -= Vector2f.Up * 5;
            Entity bolt = new Entity("", Transform.LocalPosition);
            bolt.AddComponent<BoltComponent>();
            bolt.Transform.Angle = Mathf.RandomFloat(-5,5) - GetComponent<PhysicsComponent>().Velocity.X*3.5f -90;
            Entity.Scene.AddEntity(bolt);
            AudioPlayer.Get("bolt.wav").Play(0.2f);
        }

        public override void Using()
        {
        }
    }
}
