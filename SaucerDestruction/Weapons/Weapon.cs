﻿using NuroseMobile.System;
using NuroseMobile.System.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaucerDestruction
{
    public abstract class Weapon : Component
    {
        public string IconPath { get; set; }
        public string Name { get; set; }
        public bool IsUsing { get; set; }
        public abstract void StartUse();
        public abstract void Using();
        public abstract void EndUse();
    }
}
