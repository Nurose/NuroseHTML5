﻿using NuroseMobile.Audio;
using NuroseMobile.Audio.Components;
using NuroseMobile.Graphics;
using NuroseMobile.System;
using NuroseMobile.System.Components;
using NuroseMobile.Types;
using NuroseMobile.Utilities;

namespace SaucerDestruction
{
    public class LaserBeam : Weapon
    {
        private const string laserBeamImage = "laserBeam.png";

        private string[] laserHits = { "laserHit/laserHit0.png", "laserHit/laserHit1.png", "laserHit/laserHit2.png", "laserHit/laserHit3.png", "laserHit/laserHit4.png", };

        private Vector2f hit;
        SpeakerComponent speaker;
        private void Start()
        {
            Name = "Laser Beam";
            IconPath = "laserBeamIcon.png";
            speaker = Entity.AddComponent<SpeakerComponent>();
            speaker.Loop = true;
            speaker.Volume = 0.25f;
            speaker.PlayOnStart = false;
            speaker.AudioFilePath = "laser.wav";
        }

        public override void StartUse()
        {
            GameController.Main.AddScreenShake(3.2f);
            AudioPlayer.Get("laserStart.wav").Play(0.2f);
            speaker.Play();
        }

        public override void Using()
        {
            //besms script
            float h = (float)Mathf.Tan(Transform.Angle) * Mathf.Abs(Transform.LocalPosition.Y - 452);
            hit = new Vector2f(-h + Transform.LocalPosition.X, 452);
            GameController.Main.AddScreenShake(0.025f);
            GetComponent<PhysicsComponent>().Velocity += 2 * new Vector2f(Mathf.RandomFloat(-.5f, .5f), Mathf.RandomFloat(-.5f, .5f));
            var everyone = Entity.Scene.GetAllComponentsOfType<CarController>();
            foreach (CarController car in everyone)
            {
                if ((car.Transform.LocalPosition - hit).Magnitude < 50)
                {
                    car.PhysicsComponent.Velocity += new Vector2f(Mathf.RandomFloat(-1, 1), Mathf.RandomFloat(0, -1));
                    car.PhysicsComponent.RotationalVelocity += Mathf.RandomFloat(-3, 3);
                    car.Damage(Mathf.RandomFloat(5, 25));
                }
            }
        }

        public override void EndUse()
        {
            speaker.Stop();
        }

        private void Render()
        {
            if (IsUsing)
            {
                Vector2f size = new Vector2f(Mathf.RandomFloat(58f, 120), 101) * Mathf.Clamp((Mathf.RandomFloat(1f, 1.2f) * 300) / (hit - Transform.LocalPosition).Magnitude, 0.5f, 1.5f);
                Renderer.DrawImage(ImageResourceManager.Load(laserHits[(int)Mathf.RandomFloat(0, laserHits.Length)]), hit, size, 0, new Vector2f(size.X / -2f, -size.Y), depth: -5, flipV: Mathf.RandomFloat() > .5f);

                Renderer.DrawImage(ImageResourceManager.Load(laserBeamImage), Transform.LocalPosition, new Vector2f(12, 560), Transform.Angle, new Vector2f(-.5f * 12, 3));
            }
        }
    }
}
