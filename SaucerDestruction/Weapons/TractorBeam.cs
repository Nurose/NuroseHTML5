﻿using NuroseMobile.Audio.Components;
using NuroseMobile.Graphics;
using NuroseMobile.System;
using NuroseMobile.System.Components;
using NuroseMobile.Types;
using NuroseMobile.Utilities;
using System.Collections.Generic;

namespace SaucerDestruction
{
    public class TractorBeam : Weapon
    {
        private const string tractorBeamImage = "tractorBeam.png";
        private List<PhysicsComponent> holding = new List<PhysicsComponent>();
        float opacity = 0;
        private SpeakerComponent speaker;

        private void Start()
        {
            Name = "Tractor Beam";
            IconPath = "tractorBeamIcon.png";
            speaker = Entity.AddComponent<SpeakerComponent>();
            speaker.Loop = true;
            speaker.Volume = 1f;
            speaker.PlayOnStart = false;
            speaker.AudioFilePath = "tractorBeam.wav";
        }

        public override void StartUse()
        {
            holding.Clear();
            speaker.Play();
        }

        public override void Using()
        {
            var vel = Entity.GetComponent<PhysicsComponent>().Velocity;
            var everyone = Entity.Scene.GetAllComponentsOfType<PhysicsComponent>();
            foreach (PhysicsComponent item in everyone)
            {
                if (item.Transform == Transform) continue;
                if (holding.Contains(item)) continue;
                if (Mathf.Abs(item.Transform.LocalPosition.X - Transform.LocalPosition.X) < 30 && item.Transform.LocalPosition.Y > Transform.LocalPosition.Y)
                {
                    item.RotationalVelocity = Mathf.RandomFloat(-5, 5);
                    item.UseGravity = false;
                    holding.Add(item);
                }
            }

            foreach (PhysicsComponent item in holding)
            {
                item.Velocity *= 0.97f;
                float up = Transform.LocalPosition.Y + 70 - item.Transform.LocalPosition.Y;
                up = Mathf.Abs(up)>1 ? (up > 0 ? 1f : -1f) : 0;
                item.Velocity = new Vector2f((Transform.LocalPosition.X - item.Transform.LocalPosition.X) * 0.15f, up);
                item.Velocity += vel * .7f;
                if (item.Transform.LocalPosition.Y < Transform.LocalPosition.Y) item.UseGravity = true;
            }
            holding.RemoveAll(i => (i.Transform.LocalPosition.Y < Transform.LocalPosition.Y));
        }

        public override void EndUse()
        {
            speaker.Stop();
            foreach (var item in holding)
            {
                item.UseGravity = true;
            }
            holding.Clear();
        }

        private void Render()
        {
            opacity = Mathf.Lerp(opacity, IsUsing ? 1 : 0, 0.5f);
            if (opacity > 0.05)
                Renderer.DrawImage(ImageResourceManager.Load(tractorBeamImage), Transform.LocalPosition, new Vector2f(112, 554), Transform.Angle / 2, new Vector2f(-.5f * 112, 0), opacity);
        }
    }
}
