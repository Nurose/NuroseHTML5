﻿using NuroseMobile.Audio.Components;
using NuroseMobile.Graphics;
using NuroseMobile.Graphics.Components;
using NuroseMobile.System;
using NuroseMobile.System.Components;
using NuroseMobile.Types;
using NuroseMobile.Utilities;
using System;

namespace SaucerDestruction
{
    [SingleComponent]
    public class GameController : Component
    {
        public static GameController Main;

        public PlayerController Player { get; private set; }
        public float FollowSpeed { get; set; }
        public Transform ToFollow { get; set; }
        public float ShakeDampening { get; set; } = 0.8f;

        private Vector2f cameraTranslation = Vector2f.Zero;
        private float currentScreenShake = 0;
        public int Score { get; private set; }
        public void AddScreenShake(float screenShake)
        {
            currentScreenShake += screenShake * 5f;
        }

        private void Start()
        {
            Main = this;
            Player = ToFollow.GetComponent<PlayerController>();

            var speaker = Entity.AddComponent<SpeakerComponent>();
            speaker.AudioFilePath = "46_GameMusic.mp3";
            speaker.Loop = true;

            AddScoreDisplay();

            for (int i = 0; i < 32; i++)
                SpawnCar(Mathf.RandomFloat());
        }
        private TextRenderer scoreLabel;
        private void AddScoreDisplay()
        {
            Entity scoreDisplay = new Entity("", new Vector2f(8, 64 + 10 + 16));
            scoreDisplay.Transform.LocalSize = new Vector2f(256, 64);
            scoreLabel = scoreDisplay.AddComponent<TextRenderer>();
            scoreLabel.Text = "0 points";
            scoreLabel.FontStyle = "16px Roboto";
            scoreLabel.Depth = float.NegativeInfinity;
            scoreLabel.IgnoreCameraPosition = true;
            scoreLabel.Color = Color.White;
            Entity.Scene.AddEntity(scoreDisplay);
        }

        public void AddScore(int toAdd)
        {
            Score += toAdd;
            scoreLabel.Text = Score + " points";
        }

        private void Update()
        {
            FollowTarget();
            HandleScreenShake();
            if (Time.Frame % 50 == 0)
                SpawnCar();

            if (Score < 200) return;
            //Police
            if (Time.Frame % 200 == 0)
                SpawnPoliceCar();

            if (Score < 500) return;
            //Military
            if (Time.Frame % 300 == 0)
                SpawnMilitaryCar();

            if (Score < 1000) return;
            if (Time.Frame % 1500 == 0)
                SpawnTankCar();

            //Whatever the fuck else
        }

        private void SpawnCar(float offset = 0)
        {
            bool flipped = Mathf.RandomFloat() > 0.5f;
            if (flipped) offset = 1 - offset;
            bool isTruck = Mathf.RandomFloat() > 0.8f;
            Entity car = new Entity();
            car.AddComponent<PhysicsComponent>().Mass = isTruck ? 1.5f : 1;
            car.Transform.LocalPosition = new Vector2f(Mathf.Lerp(-2548, 2548, offset), 448);
            car.Transform.LocalSize = isTruck ? new Vector2f(93, 39) : new Vector2f(44, 21);
            var spriteRenderer = car.AddComponent<SpriteRenderer>();
            car.Transform.Origin = Vector2f.One / 2f;
            var vehicleController = car.AddComponent<CarController>();
            vehicleController.Image = isTruck ? "truck.png" : "car.png";
            vehicleController.DamagedImage = isTruck ? "truckDamaged.png" : "carDamaged.png";
            vehicleController.Health = isTruck ? 150 : 100;
            vehicleController.KillScore = isTruck ? 6 : 5;
            vehicleController.Flipped = flipped;
            Entity.Scene.AddEntity(car);
        }

        private void SpawnPoliceCar()
        {
            bool flipped = Mathf.RandomFloat() > 0.5f;
            Entity car = new Entity();
            car.AddComponent<PhysicsComponent>();
            car.Transform.LocalPosition = new Vector2f(flipped ? 2548 : -2548, 448);
            car.Transform.LocalSize = new Vector2f(44, 21);
            var spriteRenderer = car.AddComponent<SpriteRenderer>();
            car.Transform.Origin = Vector2f.One / 2f;
            var vehicleController = car.AddComponent<PoliceCarController>();
            vehicleController.Image = "policeCar0.png";
            vehicleController.DamagedImage = "policeCarDamage.png";
            vehicleController.Health = 125;
            vehicleController.KillScore = 8;
            vehicleController.Flipped = flipped;
            Entity.Scene.AddEntity(car);
        }

        private void SpawnMilitaryCar()
        {
            bool flipped = Mathf.RandomFloat() > 0.5f;
            Entity car = new Entity();
            car.AddComponent<PhysicsComponent>();
            car.Transform.LocalPosition = new Vector2f(flipped ? 2548 : -2548, 448);
            car.Transform.LocalSize = new Vector2f(54, 28);
            var spriteRenderer = car.AddComponent<SpriteRenderer>();
            car.Transform.Origin = Vector2f.One / 2f;
            var vehicleController = car.AddComponent<MilitaryCarController>();
            vehicleController.Image = "militaryCar.png";
            vehicleController.DamagedImage = "militaryCarDamaged.png";
            vehicleController.Health = 170;
            vehicleController.KillScore = 15;
            vehicleController.Flipped = flipped;
            Entity.Scene.AddEntity(car);
        }

        private void SpawnTankCar()
        {
            bool flipped = Mathf.RandomFloat() > 0.5f;
            Entity car = new Entity();
            car.AddComponent<PhysicsComponent>().Mass = 5;
            car.Transform.LocalPosition = new Vector2f(flipped ? 2548 : -2548, 448);
            car.Transform.LocalSize = new Vector2f(109, 38);
            var spriteRenderer = car.AddComponent<SpriteRenderer>();
            car.Transform.Origin = Vector2f.One / 2f;
            var vehicleController = car.AddComponent<TankController>();
            vehicleController.Image = "tankBody.png";
            vehicleController.DamagedImage = "tankDamaged.png";
            vehicleController.Health = 1500;
            vehicleController.KillScore = 150;
            vehicleController.Flipped = flipped;
            Entity.Scene.AddEntity(car);
        }

        private void HandleScreenShake()
        {
            Entity.Scene.Panning = cameraTranslation + new Vector2f(Mathf.RandomFloat(-1, 1), Mathf.RandomFloat(-1, 1)) * (float)Math.Pow(currentScreenShake, .5f);
            currentScreenShake *= ShakeDampening;
        }

        private void FollowTarget()
        {
            if (ToFollow != null)
            {
                cameraTranslation = Vector2f.Lerp(cameraTranslation, ToFollow.LocalPosition - (Vector2f)(Renderer.Resolution / 2), Time.GetTimeIndependencyMultiplier() * 0.2f);
                cameraTranslation = new Vector2f(Mathf.Clamp(cameraTranslation.X, -2040, 1260), 0);
            }
        }

        public void SpawnExplosion(Vector2f position)
        {
            Entity explosion = new Entity();
            explosion.Transform.LocalPosition = position;
            explosion.AddComponent<AnimatedSpriteRenderer>();
            explosion.AddComponent<ExplosionComponent>();
            Entity.Scene.AddEntity(explosion);
        }

        public void SpawnAlienExplosion(Vector2f position)
        {
            Entity explosion = new Entity();
            explosion.Transform.LocalPosition = position;
            explosion.AddComponent<AnimatedSpriteRenderer>();
            explosion.AddComponent<AlienExplosionComponent>();
            Entity.Scene.AddEntity(explosion);
        }

        private void Render()
        {
            // Renderer.DrawImage(ImageResourceManager.Load("cityScape.png"), new Vector2f(-2048 - Entity.Scene.Panning.X*.6f, 100), new Vector2f(4096,450), depth: float.MaxValue, ignoreCamera: true, opacity: .9f);
            Renderer.DrawImage(ImageResourceManager.Load("cityScape.png"), new Vector2f(-2048 - Entity.Scene.Panning.X * .3f, Entity.Scene.Panning.Y * -.3f), new Vector2f(4096, 450), depth: float.MaxValue - 1, ignoreCamera: true);
            Renderer.DrawImage(ImageResourceManager.Load("foreground.png"), new Vector2f(-2048 - Entity.Scene.Panning.X * .5f, Entity.Scene.Panning.Y * -.5f), new Vector2f(4096, 450), depth: float.MaxValue - 2, ignoreCamera: true);
        }
    }
}