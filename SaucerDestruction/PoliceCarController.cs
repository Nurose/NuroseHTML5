﻿using NuroseMobile.System;
using NuroseMobile.System.Components;
using NuroseMobile.Types;
using NuroseMobile.Utilities;
using System;

namespace SaucerDestruction
{
    public class PoliceCarController : CarController
    {
        private float maxDistance = Mathf.RandomFloat(15, 300);
        float t = 0;
        protected override void Drive()
        {
            renderer.Depth = -0.5f;
            float delta = player.Transform.LocalPosition.X - Transform.LocalPosition.X;
            if (!(onGround && Health > 50 && Mathf.Abs(Transform.Angle) < 10)) return;//if alive

            t += (float)Time.DeltaTime;
            if (t > 0.05f)
            {
                t = 0;
                renderer.ImageSource = (renderer.ImageSource == "policeCar0.png") ? "policeCar1.png" : "policeCar0.png";
            }

            if (Mathf.Abs(delta) > maxDistance)
            {
                PhysicsComponent.Velocity += Vector2f.Right * (Speed * 2f * (delta > 0 ? 1 : -1) - PhysicsComponent.Velocity.X) * Time.GetTimeIndependencyMultiplier() * .5f;
            }
            else
            {
                PhysicsComponent.Velocity = new Vector2f(PhysicsComponent.Velocity.X * 0.9f, PhysicsComponent.Velocity.Y);
            }

            if (Math.Abs(delta) < 301)
                if (Mathf.RandomFloat() > 0.99f)
                    Shoot();
        }

        private void Shoot()
        {
            Entity bullet = new Entity("b", Transform.LocalPosition);
            bullet.AddComponent<BulletComponent>();
            var dir = (player.Transform.LocalPosition - Transform.LocalPosition);
            bullet.Transform.Angle = (float)Math.Atan2(dir.Y, dir.X) / (float)Mathf.DegreeToRadian;
            Entity.Scene.AddEntity(bullet);
        }
    }
}