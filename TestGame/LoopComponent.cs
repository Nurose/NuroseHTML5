﻿using NuroseMobile.Graphics;
using NuroseMobile.System;
using NuroseMobile.System.Components;

namespace TestGame
{
    [SingleComponent]
    class LoopComponent : Component
    {
        private void Update()
        {
            var temp = Transform.LocalPosition;

            if (temp.X > Renderer.Resolution.X) temp.X = -Transform.LocalSize.X;
            if (temp.X < -Transform.LocalSize.X) temp.X = Renderer.Resolution.X;

            if (temp.Y > Renderer.Resolution.Y) temp.Y = -Transform.LocalSize.Y;
            if (temp.Y < -Transform.LocalSize.Y) temp.Y = Renderer.Resolution.Y;

            Transform.LocalPosition = temp;
        }
    }
}
