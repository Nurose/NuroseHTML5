﻿using NuroseMobile.Graphics;
using NuroseMobile.Graphics.Components;
using NuroseMobile.System;
using NuroseMobile.Types;
using NuroseMobile.Utilities;

namespace TestGame
{
    public class App
    {
        static Scene main;
        public static void Main()
        {
            Debug.Log("WASD to move around");

            Game game = new Game();
            Renderer.Resolution = new Vector2i(800, 600);

            main = game.CreateScene("main");
            main.BackgroundColor = Color.Black;

            CreatePlayer(Renderer.Resolution / 2);
            CreateEnemy(new Vector2f(Mathf.RandomFloat(0, Renderer.Resolution.X), Mathf.RandomFloat(0, Renderer.Resolution.Y)));
            CreateEnemy(new Vector2f(Mathf.RandomFloat(0, Renderer.Resolution.X), Mathf.RandomFloat(0, Renderer.Resolution.Y)));

            game.Start();
        }

        private static void CreatePlayer(Vector2f pos)
        {
            var test = main.AddEntity(new Entity("Player", pos));
            test.AddComponent<SpriteRenderer>().ImageSource = "entity.png";
            test.AddComponent<PlayerComponent>();
        }

        private static void CreateEnemy(Vector2f pos)
        {
            var test = main.AddEntity(new Entity("Enemy", pos));
            test.AddComponent<SpriteRenderer>().ImageSource = "entity.png";
            test.AddComponent<EnemyController>();
        }
    }
}