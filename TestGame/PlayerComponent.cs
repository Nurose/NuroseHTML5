﻿using NuroseMobile.Graphics.Components;
using NuroseMobile.System;
using NuroseMobile.System.Components;
using NuroseMobile.Types;
using System;

namespace TestGame
{
    [SingleComponent]
    class PlayerComponent : Component
    {
        private float speed = 5;

        private void Start()
        {

        }

        private void Update()
        {
            if (Input.IsKeyHeld(Key.KEY_W)) Transform.LocalPosition += Vector2f.Down * speed;
            if (Input.IsKeyHeld(Key.KEY_A)) Transform.LocalPosition += Vector2f.Left * speed;
            if (Input.IsKeyHeld(Key.KEY_S)) Transform.LocalPosition += Vector2f.Up * speed;
            if (Input.IsKeyHeld(Key.KEY_D)) Transform.LocalPosition += Vector2f.Right * speed;
        }
    }
}
