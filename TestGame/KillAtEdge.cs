﻿using NuroseMobile.Graphics;
using NuroseMobile.System;
using NuroseMobile.System.Components;

namespace TestGame
{
    [SingleComponent]
    class KillAtEdge : Component
    {
        private void Update()
        {
            if (Transform.LocalPosition.X < 0 || Transform.LocalPosition.X > Renderer.Resolution.X || Transform.LocalPosition.Y < 0 || Transform.LocalPosition.Y > Renderer.Resolution.Y)
                Entity.Destroy();
        }
    }
}
