﻿using NuroseMobile.Graphics.Components;
using NuroseMobile.System;
using NuroseMobile.System.Components;
using NuroseMobile.Types;
using System;

namespace TestGame
{
    class EnemyController : Component
    {
        void Update()
        {
            Entity bullet = new Entity();
            bullet.Transform.LocalSize = new Vector2f(4, 4);
            bullet.Transform.LocalPosition = Transform.LocalPosition;
            var rect = bullet.AddComponent<RectangleRenderer>();
            rect.Color = Color.White;
            float o = (Time.Frame % 2 == 0) ? 180 : 0;
            float angle = (float)(((Time.CurrentSecond * Time.CurrentSecond) + o) % 360);
            angle *= 0.0174533f;
            bullet.AddComponent<BulletComponent>().direction = new Vector2f((float)Math.Cos(angle), (float)Math.Sin(angle));
            bullet.AddComponent<KillAtEdge>();
            Entity.Scene.AddEntity(bullet);
        }
    }
}
