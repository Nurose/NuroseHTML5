﻿using NuroseMobile.Graphics;
using NuroseMobile.System;
using NuroseMobile.System.Components;
using NuroseMobile.Types;
using System.Linq;

namespace TestGame
{
    [SingleComponent]
    class BulletComponent : Component
    {
        public Vector2f direction;
        private double life;

        private void Update()
        {
            life += Time.DeltaTime;

            Transform.LocalPosition += direction * 5;
        }
    }
}
