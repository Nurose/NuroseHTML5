﻿using NuroseMobile.Utilities;

namespace NuroseMobile.Types
{
    public struct Bounds
    {
        public Vector2f TopLeft;
        public Vector2f GetBottomRight => TopLeft + Size;
        public Vector2f Size;

        public Bounds(Vector2f topLeft, Vector2f size)
        {
            TopLeft = topLeft;
            Size = size;
        }

        public bool ContainsPoint(Vector2f v)
        {
            return (v.X > TopLeft.X && v.X < TopLeft.X + Size.X && v.Y > TopLeft.Y && v.Y < TopLeft.Y + Size.Y);
        }

        public bool Intersects(Bounds other)
        {
            return TopLeft.X < other.GetBottomRight.X && GetBottomRight.X > other.TopLeft.X && TopLeft.Y < other.GetBottomRight.Y && GetBottomRight.Y > other.TopLeft.Y;
            //return (RectA.X1 < RectB.X2 && RectA.X2 > RectB.X1 && RectA.Y1 > RectB.Y2 && RectA.Y2 < RectB.Y1);
        }

        public Vector2f RandomPoint()
        {
            return TopLeft + new Vector2f(Mathf.RandomFloat(0, Size.X), Mathf.RandomFloat(0, Size.Y));
        }
    }
}
