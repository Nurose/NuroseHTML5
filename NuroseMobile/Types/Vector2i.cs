﻿using System;

namespace NuroseMobile.Types
{
    public struct Vector2i
    {
        public Vector2i(int x, int y) : this()
        {
            X = x;
            Y = y;
        }

        public int X { get; set; }
        public int Y { get; set; }

        public float Magnitude
        {
            get
            {
                return (float)Math.Sqrt(X * X + Y * Y);
            }
        }

        public Vector2i Normalised()
        {
            float magn = Magnitude;
            return new Vector2i((int)(X / magn), (int)(Y / magn));
        }

        public void Normalise()
        {
            float magn = Magnitude;
            X = (int)(X / magn);
            Y = (int)(Y / magn);
        }

        public static readonly Vector2i Zero = new Vector2i(0, 0);
        public static readonly Vector2i One = new Vector2i(1, 1);
        public static readonly Vector2i Right = new Vector2i(1, 0);
        public static readonly Vector2i Left = new Vector2i(-1, 0);
        public static readonly Vector2i Up = new Vector2i(0, 1);
        public static readonly Vector2i Down = new Vector2i(0, -1);
        private float x;
        private float y;

        public static Vector2i operator +(Vector2i v1, Vector2i v2)
        {
            v1.X += v2.X;
            v1.Y += v2.Y;
            return v1;
        }

        public static Vector2i operator -(Vector2i v1, Vector2i v2)
        {
            v1.X -= v2.X;
            v1.Y -= v2.Y;
            return v1;
        }

        public static Vector2i operator -(Vector2i v)
        {
            return Zero - v;
        }

        public static float operator *(Vector2i v1, Vector2i v2)
        {
            return Dot(v1, v2);
        }

        public static float Dot(Vector2i v1, Vector2i v2)
        {
            return ((v1.X * v2.X) + (v1.Y * v2.Y));
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Vector2i))
            {
                return false;
            }

            var f = (Vector2i)obj;
            return X == f.X &&
                   Y == f.Y;
        }

        public override int GetHashCode()
        {
            var hashCode = 1861411795;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }

        public static Vector2i operator /(Vector2i v1, Vector2i v2)
        {
            v1.X /= v2.X;
            v1.Y /= v2.Y;
            return v1;
        }

        public static Vector2i operator *(Vector2i v1, int f)
        {
            v1.X *= f;
            v1.Y *= f;
            return v1;
        }

        public static Vector2i operator /(Vector2i v1, int f)
        {
            v1.X /= f;
            v1.Y /= f;
            return v1;
        }

        public static Vector2i operator *(int f, Vector2i v1)
        {
            v1.X *= f;
            v1.Y *= f;
            return v1;
        }

        public static Vector2i operator /(int f, Vector2i v1)
        {
            v1.X /= f;
            v1.Y /= f;
            return v1;
        }

        public static bool operator ==(Vector2i v1, Vector2i v2)
        {
            return v1.X == v2.X && v1.Y == v2.Y;
        }

        public static bool operator !=(Vector2i v1, Vector2i v2)
        {
            return !(v1 == v2);
        }

        public static implicit operator Vector2f(Vector2i v)
        {
            return new Vector2f(v.X, v.Y);
        }
    }
}
