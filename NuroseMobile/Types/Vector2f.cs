﻿using NuroseMobile.Utilities;
using System;

namespace NuroseMobile.Types
{
    public struct Vector2f
    {
        public Vector2f(float x, float y) : this()
        {
            X = x;
            Y = y;
        }

        public float X { get; set; }
        public float Y { get; set; }

        public float Magnitude
        {
            get
            {
                return (float)Math.Sqrt(X * X + Y * Y);
            }
        }

        public Vector2f Normalised
        {
            get
            {
                float magn = Magnitude;
                return new Vector2f(X / magn, Y / magn);
            }
        }

        public void Normalise()
        {
            float magn = Magnitude;
            X /= magn;
            Y /= magn;
        }

        public static readonly Vector2f Zero = new Vector2f(0, 0);
        public static readonly Vector2f One = new Vector2f(1, 1);
        public static readonly Vector2f Right = new Vector2f(1, 0);
        public static readonly Vector2f Left = new Vector2f(-1, 0);
        public static readonly Vector2f Up = new Vector2f(0, 1);
        public static readonly Vector2f Down = new Vector2f(0, -1);

        public static Vector2f operator +(Vector2f v1, Vector2f v2)
        {
            v1.X += v2.X;
            v1.Y += v2.Y;
            return v1;
        }

        public static Vector2f operator -(Vector2f v1, Vector2f v2)
        {
            v1.X -= v2.X;
            v1.Y -= v2.Y;
            return v1;
        }

        public static Vector2f operator -(Vector2f v)
        {
            return Zero - v;
        }

        public static float operator *(Vector2f v1, Vector2f v2)
        {
            return Dot(v1, v2);
        }

        public static float Dot(Vector2f v1, Vector2f v2)
        {
            return v1.X * v2.X + v1.Y * v2.Y;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Vector2f))
            {
                return false;
            }

            var f = (Vector2f)obj;
            return X == f.X &&
                   Y == f.Y;
        }

        public override int GetHashCode()
        {
            var hashCode = 1861411795;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }

        public static Vector2f operator /(Vector2f v1, Vector2f v2)
        {
            v1.X /= v2.X;
            v1.Y /= v2.Y;
            return v1;
        }

        public static Vector2f operator *(Vector2f v1, float f)
        {
            v1.X *= f;
            v1.Y *= f;
            return v1;
        }

        public static Vector2f operator /(Vector2f v1, float f)
        {
            v1.X /= f;
            v1.Y /= f;
            return v1;
        }

        public static Vector2f operator *(float f, Vector2f v1)
        {
            v1.X *= f;
            v1.Y *= f;
            return v1;
        }

        public static Vector2f operator /(float f, Vector2f v1)
        {
            v1.X /= f;
            v1.Y /= f;
            return v1;
        }

        public static bool operator ==(Vector2f v1, Vector2f v2)
        {
            return v1.X == v2.X && v1.Y == v2.Y;
        }

        public static bool operator !=(Vector2f v1, Vector2f v2)
        {
            return !(v1 == v2);
        }

        public static implicit operator Vector2i(Vector2f v)
        {
            return new Vector2i((int)v.X, (int)v.Y);
        }

        public static Vector2f Lerp(Vector2f a, Vector2f b, float c)
        {
            return new Vector2f(Mathf.Lerp(a.X, b.X, c), Mathf.Lerp(a.Y, b.Y, c));
        }

        public static Vector2f FromAngle(float degrees, float distance)
        {
            return distance * new Vector2f(Mathf.Cos(degrees), Mathf.Sin(degrees ));
        }
    }
}
