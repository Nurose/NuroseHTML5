﻿namespace NuroseMobile.Types
{
    public struct Color
    {
        public Color(byte r, byte g, byte b, float a = 1) : this()
        {
            R = r;
            G = g;
            B = b;
            A = a;
        }

        public byte R { get; set; }
        public byte G { get; set; }
        public byte B { get; set; }
        public float A { get; set; }

        public static Color Red => new Color(255, 0, 0);
        public static Color Green => new Color(0, 255, 0);
        public static Color Blue => new Color(0, 0, 255);
        public static Color Black => new Color(0, 0, 0);
        public static Color White => new Color(255, 255, 255);


        public string ToCssString()
        {
            return "rgba(" + R + "," + G + "," + B + "," + A + ")";
        }
    }
}
