﻿using Bridge;
using System;

namespace NuroseMobile.Utilities
{
    public static class Mathf
    {
        public const double DegreeToRadian = Math.PI / 180d;
        private static Random rand = new Random();

        /// <summary>
        /// Returns random float
        /// </summary>   
        public static float RandomFloat(float min = 0f, float max = 1f)
        {
            return Lerp(min, max, (float)rand.NextDouble());
        }

        /// <summary>
        /// Returns random int. Exclusive max
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static float RandomInt(int min = 0, float max = 2)
        {
            return (float)Math.Round(Lerp(min, max-1, (float)rand.NextDouble()));
        }

        public static float Lerp(float a, float b, float c)
        {
            return a * (1.0f - c) + b * c;
        }

        public static float Clamp(float value, float min, float max)
        {
            if (value < min) return min;
            if (value > max) return max;
            return value;
        }

        public static float Sin(float angle)
        {
            return (float)Math.Sin(angle * DegreeToRadian);
        }

        public static float Cos(float angle)
        {
            return (float)Math.Cos(angle * DegreeToRadian);
        }

        public static float Tan(float angle)
        {
            return (float)Math.Tan(angle * DegreeToRadian);
        }

        public static int Clamp(int value, int min, int max)
        {
            if (value < min) return min;
            if (value > max) return max;
            return value;
        }

        public static float Abs(float v)
        {
            if (v < 0) return -v;
            return v;
        }

        public static int Abs(int v)
        {
            if (v < 0) return -v;
            return v;
        }

        public static float Max(float v1, float v2)
        {
            return (v1 > v2) ? v1 : v2;
        }

        public static float Min(float v1, float v2)
        {
            return (v1 < v2) ? v1 : v2;
        }
    }
}
