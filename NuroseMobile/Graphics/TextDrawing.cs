﻿using Bridge.Html5;
using NuroseMobile.Types;

namespace NuroseMobile.Graphics
{
    internal class TextDrawing : Drawing
    {
        public TextDrawing(string text, string font, Vector2f topLeft, Vector2f size, Vector2f pivot, float opacity, float angle, bool ignoreCamera, float depth) : base(topLeft, pivot, opacity, angle, ignoreCamera, depth)
        {
            Text = text;
            Font = font;
            Size = size;
        }

        public string Text { get; }
        public string Font { get; }
        public Vector2f Size { get; }

        public override void Draw(CanvasRenderingContext2D context, Vector2f offset)
        {
            if (IgnoreCamera)
                context.SetTransform(1, 0, 0, 1, TopLeft.X, TopLeft.Y);
            else
                context.SetTransform(1, 0, 0, 1, TopLeft.X - offset.X, TopLeft.Y - offset.Y);

            context.Rotate(Angle * Renderer.DEGREETORADIAN);
            context.GlobalAlpha = Opacity;
            context.Font = Font;

            context.FillText(Text, (int)Pivot.X, (int)Pivot.Y, (int)Size.X);
        }
    }
}
