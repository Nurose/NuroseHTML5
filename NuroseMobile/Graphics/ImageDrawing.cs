﻿using Bridge.Html5;
using NuroseMobile.Types;

namespace NuroseMobile.Graphics
{
    internal class ImageDrawing : Drawing
    {
        public ImageDrawing(HTMLImageElement htmlImageElement, Vector2f topLeft, Vector2f size, Vector2f pivot, float opacity, float angle, bool ignoreCamera, float depth, bool flipV, bool flipH) : base(topLeft, pivot, opacity, angle, ignoreCamera, depth)
        {
            HTMLImageElement = htmlImageElement;
            Size = size;
            FlipV = flipV;
            FlipH = flipH;
        }

        public Vector2f Size { get; }
        public bool FlipV { get; }
        public bool FlipH { get; }
        public HTMLImageElement HTMLImageElement { get; }

        public override void Draw(CanvasRenderingContext2D context, Vector2f offset)
        {
            if (IgnoreCamera)
                context.SetTransform(1, 0, 0, 1, TopLeft.X, TopLeft.Y);
            else
                context.SetTransform(1, 0, 0, 1, TopLeft.X - offset.X, TopLeft.Y - offset.Y);

            context.Rotate(Angle * Renderer.DEGREETORADIAN);
            context.Scale(FlipV ? -1 : 1, FlipH ? -1 : 1);
            context.GlobalAlpha = Opacity;
            context.DrawImage(HTMLImageElement, Pivot.X, Pivot.Y, Size.X, Size.Y);
            context.Scale(1, 1);
        }
    }
}
