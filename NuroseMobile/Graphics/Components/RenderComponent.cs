﻿using NuroseMobile.System;
using NuroseMobile.System.Components;
using NuroseMobile.Types;

namespace NuroseMobile.Graphics.Components
{
    /// <summary>
    /// Abstract component used to draw to the screen
    /// </summary>
    public abstract class RenderComponent : Component
    {
        /// <summary>
        /// A value that ranges from 0.0 to 1.0 that determines the opacity of the object
        /// </summary>
        public float Opacity { get; set; } = 1.0f;
        /// <summary>
        /// Whether or not to ignore the camera's position. Useful for drawing UI elements. Default is false
        /// </summary>
        public bool IgnoreCameraPosition { get; set; }
        /// <summary>
        /// The depth of the drawn object. The higher, the further back it is drawn
        /// </summary>
        public float Depth { get; set; }

        protected abstract void Render();
    }
}
