﻿using NuroseMobile.System;
using NuroseMobile.Types;

namespace NuroseMobile.Graphics.Components
{
    public sealed class SpriteRenderer : RenderComponent
    {
        private bool _verticalFlip;

        /// <summary>
        /// Source path for the image
        /// </summary>
        public string ImageSource { get; set; }

        public bool VerticalFlip { get; set; }
        public bool HorizontalFlip { get; set; }

        protected override void Render()
        {
            if (string.IsNullOrWhiteSpace(ImageSource))
                return;
            var offset = new Vector2f(Transform.Origin.X * Transform.LocalSize.X, Transform.Origin.Y * Transform.LocalSize.Y);
            Renderer.DrawImage(ImageResourceManager.Load(ImageSource), Transform.LocalPosition, Transform.LocalSize, Transform.Angle, -offset, Opacity, IgnoreCameraPosition, Depth, VerticalFlip, HorizontalFlip);
        }
    }
}
