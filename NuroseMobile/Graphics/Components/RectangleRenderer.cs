﻿using NuroseMobile.System;
using NuroseMobile.Types;

namespace NuroseMobile.Graphics.Components
{
    /// <summary>
    /// A <see cref="RenderComponent"/> used to draw a rectangle in place of the <see cref="Entity"/>
    /// </summary>
    public sealed class RectangleRenderer : RenderComponent
    {
        /// <summary>
        /// <see cref="Color"/> used to draw the rectangle
        /// </summary>
        public Color Color { get; set; } = Color.Black;

        protected override void Render()
        {
            var offset = new Vector2f(Transform.Origin.X * Transform.LocalSize.X, Transform.Origin.Y * Transform.LocalSize.Y);

            Renderer.SetColor(Color);
            Renderer.DrawRectangle(Transform.LocalPosition, Transform.LocalSize, Transform.Angle, -offset, Opacity, IgnoreCameraPosition, Depth);
        }
    }
}
