﻿using NuroseMobile.System;
using NuroseMobile.Types;
using System.Collections.Generic;

namespace NuroseMobile.Graphics.Components
{
    public sealed class AnimatedSpriteRenderer : RenderComponent
    {
        private int _frame = 0;
        /// <summary>
        /// Current frame
        /// </summary>
        public int Frame
        {
            get => _frame;

            set
            {
                if (Frames.Count == 0) return;
                _frame = value;
                if (value < 0) _frame = 0;
                if (value >= Frames.Count) _frame = value % Frames.Count;
            }
        }
        /// <summary>
        /// All frames
        /// </summary>
        public List<string> Frames { get; private set; } = new List<string>();
        /// <summary>
        /// Determines if the animation is running. Default is true
        /// </summary>
        public bool Animated { get; set; } = true;
        /// <summary>
        /// Framerate of the animation. Default is 12
        /// </summary>
        public float Framerate { get; set; } = 12;
        /// <summary>
        /// Determines if the animation loops. Default is false
        /// </summary>
        public bool Loop { get; set; } = false;

        private float t = 0;
        private void Update()
        {
            if (!Animated) return;
            t += (float)Time.DeltaTime;
            if (t > (1f / Framerate))
            {
                t = 0;
                Frame++;
                if (Frame == Frames.Count-1 && !Loop)
                {
                    Animated = false;
                }
            }
        }

        protected override void Render()
        {
            if (Frames.Count == 0) return;
            var offset = new Vector2f(Transform.Origin.X * Transform.LocalSize.X, Transform.Origin.Y * Transform.LocalSize.Y);
            Renderer.DrawImage(ImageResourceManager.Load(Frames[Frame]), Transform.LocalPosition, Transform.LocalSize, Transform.Angle, -offset, Opacity, IgnoreCameraPosition, Depth);
        }
    }
}
