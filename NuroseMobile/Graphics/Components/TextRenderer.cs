﻿using NuroseMobile.Types;

namespace NuroseMobile.Graphics.Components
{
    public class TextRenderer : RenderComponent
    {
        public string Text { get; set; } = "Hello World!";
        /// <summary>
        /// Default is "12px Arial"
        /// </summary>
        public string FontStyle { get; set; } = "12px Arial";
        public Color Color { get; set; } = Color.Black;
        //public int FontSize { get; set; }

        protected override void Render()
        {
            var offset = new Vector2f(Transform.Origin.X * Transform.LocalSize.X, Transform.Origin.Y * Transform.LocalSize.Y);
            Renderer.SetColor(Color);
            Renderer.DrawText(Text, FontStyle, Transform.LocalPosition, Transform.LocalSize, Transform.Angle, -offset, Opacity, IgnoreCameraPosition, Depth);
        }
    }
}
