﻿using Bridge.Html5;
using NuroseMobile.Types;

namespace NuroseMobile.Graphics
{
    internal abstract class Drawing
    {
        protected Drawing(Vector2f topLeft, Vector2f pivot, float opacity, float angle, bool ignoreCamera, float depth)
        {
            TopLeft = topLeft;
            Pivot = pivot;
            Opacity = opacity;
            Angle = angle;
            IgnoreCamera = ignoreCamera;
            Depth = depth;
        }

        public Vector2f TopLeft { get; }
        public Vector2f Pivot { get; }
        public float Opacity { get; }
        public float Angle { get; }
        public bool IgnoreCamera { get; }

        public float Depth { get; }
        public abstract void Draw(CanvasRenderingContext2D context, Vector2f offset);
    }
}
