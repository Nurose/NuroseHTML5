﻿using Bridge.Html5;
using NuroseMobile.System;
using NuroseMobile.Types;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NuroseMobile.Graphics
{
    public static class Renderer
    {
        public const double TAU = 2d * Math.PI;
        public const double DEGREETORADIAN = Math.PI / 180d;

        /// <summary>
        /// Canvas resolution
        /// </summary>
        public static Vector2i Resolution
        {
            get
            {
                return new Vector2i(PageAccessor.Canvas.Width, PageAccessor.Canvas.Height);
            }

            set
            {
                PageAccessor.Canvas.Width = value.X;
                PageAccessor.Canvas.Height = value.Y;
            }
        }
        /// <summary>
        /// Global drawing offset
        /// </summary>
        public static Vector2f Offset { get; set; }

        private static Color drawingColor = Color.Black;
        private static CanvasRenderingContext2D context;

        private static List<Drawing> queue = new List<Drawing>();

        internal static void Initialise()
        {
            Resolution = new Vector2i(480, 360);
            context = PageAccessor.Canvas.GetContext(CanvasTypes.CanvasContext2DType.CanvasRenderingContext2D);
            context.LineWidth = 0;
            context.ImageSmoothingEnabled = false;

            Debug.Log("Renderer initialised");
        }

        public static void Render()
        {
            var sorted = queue.OrderByDescending(c => c.Depth);
            foreach (Drawing drawing in sorted)
                drawing.Draw(context, Offset);
            queue.Clear();
        }

        /// <summary>
        /// Clears the screen with the given <see cref="Color"/>
        /// </summary>
        /// <param name="color"></param>
        public static void Clear(Color color)
        {
            context.GlobalAlpha = 1;
            context.SetTransform(1, 0, 0, 1, 0, 0);
            SetColor(color);
            context.FillRect(0, 0, Resolution.X, Resolution.Y);
        }

        /// <summary>
        /// Draws a cricle
        /// </summary>
        /// <param name="topleft"></param>
        /// <param name="radius"></param>
        /// <param name="angle"></param>
        public static void DrawCircle(Vector2f topleft, float radius, float angle = 0, Vector2f pivot = default(Vector2f), float opacity = 1f, bool ignoreCamera = false)
        {
            return;
            if (ignoreCamera)
                context.SetTransform(1, 0, 0, 1, topleft.X, topleft.Y);
            else
                context.SetTransform(1, 0, 0, 1, topleft.X - Offset.X, topleft.Y - Offset.Y);

            context.Rotate(angle * DEGREETORADIAN);
            context.GlobalAlpha = opacity;
            context.BeginPath();
            context.Arc(pivot.X, pivot.Y, radius, 0, TAU);
            context.Fill(CanvasTypes.CanvasFillRule.EvenOdd);
        }
        /// <summary>
        /// Draws a rectangle
        /// </summary>
        /// <param name="topleft"></param>
        /// <param name="size"></param>
        /// <param name="angle"></param>
        public static void DrawRectangle(Vector2f topleft, Vector2f size, float angle = 0, Vector2f pivot = default(Vector2f), float opacity = 1f, bool ignoreCamera = false, float depth = 0)
        {
            queue.Add(new RectangleDrawing(topleft, size, pivot, opacity, angle, ignoreCamera, depth));
        }
        /// <summary>
        /// Draws an image from a <see cref="HTMLImageElement"/> to the canvas
        /// </summary>
        /// <param name="image"></param>
        /// <param name="topleft"></param>
        /// <param name="size"></param>
        /// <param name="angle"></param>
        /// <param name="pivot"></param>
        public static void DrawImage(HTMLImageElement image, Vector2f topleft, Vector2f size, float angle = 0, Vector2f pivot = default(Vector2f), float opacity = 1f, bool ignoreCamera = false, float depth = 0, bool flipV = false, bool flipH = false)
        {
            queue.Add(new ImageDrawing(image, topleft, size, pivot, opacity, angle, ignoreCamera, depth, flipV, flipH));
        }
        /// <summary>
        /// Draws a string to the screen
        /// </summary>
        public static void DrawText(string text, string font, Vector2f topleft, Vector2f size, float angle = 0, Vector2f pivot = default(Vector2f), float opacity = 1f, bool ignoreCamera = false, float depth = 0)
        {
            queue.Add(new TextDrawing(text, font, topleft, size, pivot, opacity, angle, ignoreCamera, depth));
        }
        /// <summary>
        /// Set current drawing color
        /// </summary>
        /// <param name="color"></param>
        public static void SetColor(Color color)
        {
            context.FillStyle = color.ToCssString();
        }
    }
}
