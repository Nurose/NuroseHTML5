﻿using Bridge.Html5;
using System.Collections.Generic;

namespace NuroseMobile.System
{
    /// <summary>
    /// Loads resources and holds them in memory
    /// </summary>

    public static class ImageResourceManager
    {
        private static Dictionary<string, HTMLImageElement> loadedResources = new Dictionary<string, HTMLImageElement>();

        public static HTMLImageElement Load(string name)
        {
            if (loadedResources.TryGetValue(name, out HTMLImageElement elem))
            {
                return elem;
            }
            else
            {
                HTMLImageElement newElem = new HTMLImageElement();
                newElem.Src = name;
                loadedResources.Add(name, newElem);
                return newElem;
            }
        }

        public static void Preload(params string[] sources)
        {
            foreach (string source in sources)
            {
                var image = PageAccessor.AddElementToBody("img") as HTMLImageElement;
                image.Src = source;
                image.Style.Display = "none";
            }
        }
    }
}
