﻿using NuroseMobile.System.Components;
using System;

namespace NuroseMobile.System
{
    public interface IComponentAccess
    {
        /// <summary>
        /// Gets the first component of the given type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        Component GetComponent(Type type);
        /// <summary>
        /// Gets the first component of the given type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        T GetComponent<T>() where T : Component;
        /// <summary>
        /// Gets all components of the given type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        Component[] GetComponents(Type type);
        /// <summary>
        /// Gets all components of the given type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        T[] GetComponents<T>() where T : Component;
    }
}
