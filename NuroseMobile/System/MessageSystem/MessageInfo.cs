﻿using System;

namespace NuroseMobile.System
{
    internal struct MessageInfo
    {
        public MessageInfo(Type type, string name) : this()
        {
            Type = type;
            Name = name;
        }

        public Type Type { get; set; }
        public string Name { get; set; }
    }
}
