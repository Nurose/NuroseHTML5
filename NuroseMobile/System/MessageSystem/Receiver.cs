﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Bridge;

namespace NuroseMobile.System
{
    public abstract class Receiver : Containable
    {
        public bool Enabled { get; set; } = true;

        private List<ICollection> collections = new List<ICollection>();

        private Dictionary<string, Action> actions = new Dictionary<string, Action>();

        protected void RegisterCollection(ICollection collection)
        {
            if (collections.Contains(collection)) return;
            collections.Add(collection);
        }

        protected void Deinitialise()
        {
            collections.Clear();
        }

        internal void ReceiveMessage(string messageName, params object[] arguments)
        {
            if (!Enabled) return;

            CallMethod(messageName, arguments);

            foreach (ICollection collection in collections)
                foreach (object obj in collection)
                {
                    ((Receiver)obj).ReceiveMessage(messageName, arguments);
                }
        }

        private void CallMethod(string messageName, params object[] arguments)
        {
            var method = ReflectionSaver.Main.GetMethod(new MessageInfo(GetType(), messageName));
            if (method == null) return;
            
            if (actions.TryGetValue(messageName, out Action del))
            {
                del.Call();
            }
            else
            {
                Action createdDel = (Action) Delegate.CreateDelegate(typeof(Action), this, method);
                actions.Add(messageName, createdDel);
                createdDel.Call();
            }
        }
    }
}
