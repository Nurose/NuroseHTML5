﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuroseMobile.System
{
    public static class MessageSystem
    {
        public static void SendMessage(Receiver receiver, string messageName, params object[] arguments)
        {
            receiver.ReceiveMessage(messageName, arguments);
        }
    }
}
