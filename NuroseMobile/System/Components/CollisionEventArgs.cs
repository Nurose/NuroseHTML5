﻿using System;

namespace NuroseMobile.System.Components
{
    public class CollisionEventArgs : EventArgs
    {
        public readonly ColliderComponent Other;

        public CollisionEventArgs(ColliderComponent other)
        {
            Other = other;
        }
    }
}
