﻿using System;

namespace NuroseMobile.System.Components
{
    public abstract class Component : Receiver, IComponentAccess
    {
        public Entity Entity
        {
            get
            {
                return Owner as Entity;
            }
        }

        public Transform Transform => Entity.Transform;

        protected Component()
        {

        }

        public Component GetComponent(Type type)
        {
            return Entity.GetComponent(type);
        }

        public T GetComponent<T>() where T : Component
        {
            return Entity.GetComponent<T>();
        }

        public override void Destroy()
        {
            Entity.RemoveComponent(this);
        }

        public Component[] GetComponents(Type type)
        {
            return Entity.GetComponents(type);
        }

        public T[] GetComponents<T>() where T : Component
        {
            return Entity.GetComponents<T>();
        }
    }
}
