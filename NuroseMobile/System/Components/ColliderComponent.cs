﻿using NuroseMobile.Types;
using System;

namespace NuroseMobile.System.Components
{
    public class ColliderComponent : Component
    {
        public event EventHandler<CollisionEventArgs> OnCollision;
        public Bounds Bounds => new Bounds(Transform.LocalPosition, Transform.LocalSize);

        private void Update()
        {
            var allColliders = Entity.Scene.GetAllComponentsOfType<ColliderComponent>();
            foreach (ColliderComponent collider in allColliders)
            {
                if (collider == this) break;
                if (Bounds.Intersects(collider.Bounds))
                    OnCollision?.Invoke(this, new CollisionEventArgs(collider));
            }
        }
    }
}
