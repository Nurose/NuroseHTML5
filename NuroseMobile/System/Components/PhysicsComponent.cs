﻿using NuroseMobile.Types;

namespace NuroseMobile.System.Components
{
    [SingleComponent]
    public class PhysicsComponent : Component
    {
        public Vector2f Velocity { get; set; }
        public float RotationalVelocity { get; set; }

        public float Dampening { get; set; } = 0.99f;
        public float RotationalDampening { get; set; } = 0.99f;

        public Vector2f Gravity { get; set; } = new Vector2f(0, .5f);
        public bool UseGravity { get; set; } = true;

        /// <summary>
        /// Default is 1
        /// </summary>
        public float Mass { get; set; } = 1;

        private void Update()
        {
            Transform.LocalPosition += Velocity*Time.GetTimeIndependencyMultiplier() / Mass;
            Transform.Angle += RotationalVelocity * Time.GetTimeIndependencyMultiplier() / Mass;

            Velocity *= Dampening;
            RotationalVelocity *= RotationalDampening;

            if (UseGravity)
                Velocity += Gravity * Mass;
        }
    }
}