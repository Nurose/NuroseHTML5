﻿using NuroseMobile.Types;

namespace NuroseMobile.System.Components
{
    [SingleComponent]
    public sealed class Transform : Component
    {
        public Vector2f LocalPosition { get; set; } = new Vector2f(0, 0);
        public Vector2f LocalSize { get; set; } = new Vector2f(32, 32);
        public float Angle { get; set; } = 0;
        /// <summary>
        /// Defines the origin with (0, 0) being the top left and (1, 1) being the bottom right. Default is (0, 0)
        /// </summary>
        public Vector2f Origin { get; set; } = new Vector2f(.0f, .0f);
        public Bounds Bounds => new Bounds(Transform.LocalPosition - new Vector2f(Transform.Origin.X * Transform.LocalSize.X, Transform.Origin.Y * Transform.LocalSize.Y), Transform.LocalSize);
    }
}
