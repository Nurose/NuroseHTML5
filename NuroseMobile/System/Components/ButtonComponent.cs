﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuroseMobile.System.Components
{
    public class ButtonComponent : Component
    {
        public Action ClickAction;

        private void Update()
        {
            if (Input.MousePosition.X > Transform.LocalPosition.X && Input.MousePosition.X < Transform.LocalPosition.X + Transform.LocalSize.X &&
                Input.MousePosition.Y > Transform.LocalPosition.Y && Input.MousePosition.Y < Transform.LocalPosition.Y + Transform.LocalSize.Y && Input.IsButtonReleased(0))
                ClickAction?.Invoke();
        }
    }
}
