﻿using NuroseMobile.System.Components;
using NuroseMobile.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NuroseMobile.System
{
    public sealed class Entity : Container<Component>, IComponentAccess
    {
        public string Name { get; set; }
        public Transform Transform { get; }
        public Scene Scene
        {
            get
            {
                return Owner as Scene;
            }
            internal set
            {
                Owner = value;
            }
        }

        public Entity(string name = "Entity", Vector2f localPosition = default(Vector2f)) : base()
        {
            Name = name;
            Transform = AddComponent<Transform>();

            Transform.LocalPosition = localPosition;
        }

        public Component GetComponent(Type type)
        {
            return AsReadOnlyList().FirstOrDefault(c => type.IsInstanceOfType(c));
        }

        public T GetComponent<T>() where T : Component
        {
            return GetComponent(typeof(T)) as T;
        }

        public Component AddComponent(Type type)
        {
            DoAttributeChecks(type);
            return AddObject(Activator.CreateInstance(type) as Component);
        }

        public T AddComponent<T>() where T : Component
        {
            return AddComponent(typeof(T)) as T;
        }

        private void DoAttributeChecks(Type type)
        {
            var attributes = type.GetCustomAttributes(true);

            SingleComponentCheck(type, attributes);
            RequiredComponentCheck(type, attributes);
        }

        private void SingleComponentCheck(Type type, object[] attributes)
        {
            if (attributes.Count(t => Type.IsInstanceOfType(t, typeof(SingleComponentAttribute))) > 0 && GetComponent(type) != null)
                throw new Exception($"An entity can only have a single {type.Name}");
        }

        private void RequiredComponentCheck(Type type, object[] attributes)
        {
            foreach (RequireComponentAttribute attr in attributes.Where(t => Type.IsInstanceOfType(t, typeof(RequireComponentAttribute))))
            {
                var requiredComponents = attr.RequiredComponents;
                foreach (Type componentType in requiredComponents)
                {
                    if (GetComponent(componentType) == null)
                        throw new Exception($"{type.Name} requires a {componentType.Name}");
                }
            }
        }

        public void RemoveComponent(Component component)
        {
            RemoveObject(component);
        }

        public void RemoveComponent(Type type)
        {
            RemoveComponent(GetComponent(type));
        }

        public void RemoveComponent<T>() where T : Component
        {
            RemoveComponent(typeof(T));
        }

        public override void Destroy()
        {
            Scene.RemoveEntity(this);
            return;
        }

        public Component[] GetComponents(Type type)
        {
            return AsReadOnlyList().Where(c=>Type.IsInstanceOfType(c, type)).ToArray();
        }

        public T[] GetComponents<T>() where T : Component
        {
            return AsReadOnlyList().OfType<T>().ToArray();
        }
    }
}
