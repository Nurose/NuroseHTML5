﻿using NuroseMobile.System.Components;
using System;

namespace NuroseMobile.System
{
    public sealed class RequireComponentAttribute : Attribute
    {
        public RequireComponentAttribute(params Type[] requiredComponents)
        {
            RequiredComponents = requiredComponents;
        }

        public Type[] RequiredComponents { get; }
    }
}
