﻿using Bridge.Html5;
using NuroseMobile.Types;
using System.Collections.Generic;

namespace NuroseMobile.System
{
    public static class Input
    {
        private static HashSet<Key> _keysHeld = new HashSet<Key>();
        private static HashSet<Key> _keysPressed = new HashSet<Key>();
        private static HashSet<Key> _keysReleased = new HashSet<Key>();

        private static HashSet<int> _buttonsHeld = new HashSet<int>();
        private static HashSet<int> _buttonsPressed = new HashSet<int>();
        private static HashSet<int> _buttonsReleased = new HashSet<int>();

        /// <summary>
        /// Mouse position relative to the canvas
        /// </summary>
        public static Vector2f MousePosition { get; private set; }
        public static float MouseWheelDelta { get; private set; }

        internal static void Initialise()
        {
            Document.OnMouseMove = OnMouseMove;
            Document.OnMouseWheel = OnMouseScroll;
            Document.OnKeyDown = OnKeyDown;
            Document.OnKeyUp = OnKeyUp;

            Document.OnMouseUp = OnButtonUp;
            Document.OnMouseDown = OnButtonDown;

            Debug.Log("Input initialised");
        }

        private static void OnMouseScroll(Event arg)
        {
            MouseWheelDelta = (float)((WheelEvent)arg).DeltaY;
        }

        private static void OnKeyUp(KeyboardEvent arg)
        {
            _keysReleased.Add((Key)arg.Which);
            _keysHeld.Remove((Key)arg.Which);
        }

        private static void OnKeyDown(KeyboardEvent arg)
        {
            if (!arg.Repeat)
                _keysPressed.Add((Key)arg.Which);
            _keysHeld.Add((Key)arg.Which);
        }

        private static void OnMouseMove(MouseEvent arg)
        {
            var rect = PageAccessor.Canvas.GetBoundingClientRect();
            MousePosition = new Vector2f(arg.ClientX - (float)rect.Left, arg.ClientY - (float)rect.Top);
        }

        private static void OnButtonUp(MouseEvent arg)
        {
            _buttonsReleased.Add(arg.Button);
            _buttonsHeld.Remove(arg.Button);
        }

        private static void OnButtonDown(MouseEvent arg)
        {
            _buttonsPressed.Add(arg.Button);
            _buttonsHeld.Add(arg.Button);
        }

        internal static void RefreshInput()
        {
            _keysPressed.Clear();
            _keysReleased.Clear();

            _buttonsPressed.Clear();
            _buttonsReleased.Clear();
        }

        public static bool IsKeyHeld(Key key)
        {
            return _keysHeld.Contains(key);
        }

        public static bool IsKeyPressed(Key key)
        {
            return _keysPressed.Contains(key);
        }

        public static bool IsKeyReleased(Key key)
        {
            return _keysReleased.Contains(key);
        }

        public static bool IsButtonHeld(int button)
        {
            return _buttonsHeld.Contains(button);
        }

        public static bool IsButtonPressed(int button)
        {
            return _buttonsPressed.Contains(button);
        }

        public static bool IsButtonReleased(int button)
        {
            return _buttonsReleased.Contains(button);
        }
    }
}
