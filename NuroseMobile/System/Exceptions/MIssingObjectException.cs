﻿namespace NuroseMobile.System.Exceptions
{
    public sealed class MissingObjectException<T> : global::System.Exception where T : Receiver
    {
        private T missing;
        private Container<T> owner;

        public MissingObjectException(T missing, Container<T> owner)
        {
            this.missing = missing;
            this.owner = owner;
        }

        public override string Message => $"This {missing.GetType().Name} was not found in this {owner.GetType().Name}";
    }
}
