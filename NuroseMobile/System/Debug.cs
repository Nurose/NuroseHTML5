﻿using System;

namespace NuroseMobile.System
{
    public static class Debug
    {
        public static void Log(object message)
        {
#if DEBUG
            Console.WriteLine("[" + DateTime.Now.ToShortTimeString() + "] " + message);
#endif
        }
    }
}
