﻿using Bridge;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace NuroseMobile.System
{

    internal class ReflectionSaver
    {
        private Dictionary<MessageInfo, MethodInfo> methods = new Dictionary<MessageInfo, MethodInfo>();

        public static ReflectionSaver Main { get; private set; }
        public ReflectionSaver()
        {
            Main = this;
            Debug.Log("Reflection saver initialised");
        }

        internal static void Initialise()
        {
            Main = new ReflectionSaver();
        }

        public MethodInfo GetMethod(MessageInfo messageInfo)
        {
            if (methods.TryGetValue(messageInfo, out MethodInfo found))
                return found;

            found = messageInfo.Type.GetMethod(messageInfo.Name ,BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            methods.Add(messageInfo, found);
            return found;
        }
    }
}
