﻿using Bridge.Html5;

namespace NuroseMobile.System
{
    public static class PageAccessor
    {
        public static HTMLCanvasElement Canvas { get; set; }

        internal static void Initialise()
        {
            CreateCanvas();
            Debug.Log("Page accessor intialised");
        }

        private static void AddScriptReference(string v)
        {
            var script = Document.CreateElement<HTMLScriptElement>("script");
            script.Src = v;
            Document.Head.AppendChild(script);
        }

        private static void CreateCanvas()
        {
            Canvas = Document.QuerySelector<HTMLCanvasElement>("canvas");
            if (Canvas == null)
            {
                Canvas = new HTMLCanvasElement();
                Document.Body.AppendChild(Canvas);
            }
        }

        public static HTMLElement AddElementToBody(string tag)
        {
            var elem = Document.CreateElement(tag);
            Document.Body.AppendChild(elem);
            return elem;
        }
    }
}
