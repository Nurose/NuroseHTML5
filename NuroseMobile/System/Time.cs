﻿using System.Diagnostics;

namespace NuroseMobile.System
{
    public static class Time
    {
        public static uint Frame { get; private set; }
        public static double CurrentSecond { get; private set; }
        public static double DeltaTime { get; private set; }
        public static float TargetFPS { get; set; } = 60;

        private static Stopwatch stopwatch = new Stopwatch();

        internal static void StartTick()
        {
            Frame++;
            CurrentSecond += DeltaTime;
        }

        internal static void EndTick()
        {
            DeltaTime = stopwatch.Elapsed.TotalSeconds;
            stopwatch.Restart();
        }


        /// <summary>
        /// Gets the mutliplier by which to multiply time-dependent values with to make them independent. Returns targetFPS * <see cref="DeltaTime"/>;
        /// </summary>
        /// <returns></returns>
        public static float GetTimeIndependencyMultiplier()
        {
            return TargetFPS * (float)DeltaTime;
        }
    }
}
