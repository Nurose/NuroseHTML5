﻿using Bridge.Html5;
using NuroseMobile.Graphics;
using NuroseMobile.Types;
using System;
using System.Collections.Generic;

namespace NuroseMobile.System
{
    public sealed class Game
    {
        private List<Scene> scenes = new List<Scene>();

        public Game(Vector2i resolution)
        {
            Initialise();
            Renderer.Resolution = resolution;
        }

        public Game()
        {
            Initialise();
        }

        private void Initialise()
        {
            PageAccessor.Initialise();
            ReflectionSaver.Initialise();
            Renderer.Initialise();
            Input.Initialise();
        }

        public Scene ActiveScene { get; set; }

        public void Start()
        {
            if (scenes.Count != 0) ActiveScene = scenes[0];
            Debug.Log("Game initialised");
            AnimationFrame();
        }

        public Scene CreateScene(string name)
        {
            Scene newScene = new Scene(name, this);
            scenes.Add(newScene);
            return newScene;
        }

        public Scene GetScene(string name)
        {
            return scenes.Find(s => s.Name == name);
        }

        private void AnimationFrame()
        {
            Time.StartTick();
            PreUpdate();
            Update();
            PostUpdate();
            Render();
            Renderer.Render();
            Input.RefreshInput();
            Time.EndTick();
            Window.RequestAnimationFrame(AnimationFrame);
        }

        private void PreUpdate()
        {
            if (ActiveScene == null) return;
            MessageSystem.SendMessage(ActiveScene, "PreUpdate");
        }

        private void Update()
        {
            if (ActiveScene == null) return;
            MessageSystem.SendMessage(ActiveScene, "Update");
        }

        private void PostUpdate()
        {
            if (ActiveScene == null) return;
            MessageSystem.SendMessage(ActiveScene, "PostUpdate");
        }

        private void Render()
        {
            if (ActiveScene == null) return;
            MessageSystem.SendMessage(ActiveScene, "Render");
        }
    }
}
