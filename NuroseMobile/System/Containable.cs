﻿
namespace NuroseMobile.System
{
    public abstract class Containable
    {
        /// <summary>
        /// The <see cref="Container{T}"/> this object is in
        /// </summary>
        internal object Owner { get; set; }

        /// <summary>
        /// Removed the <see cref="Containable"/> from its <see cref="Owner"/>
        /// </summary>
        public abstract void Destroy();
    }
}
