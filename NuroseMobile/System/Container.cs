﻿using System.Collections.Generic;
using System.Linq;
namespace NuroseMobile.System
{
    public abstract class Container<T> : Receiver where T : Receiver
    {
        private List<T> objects = new List<T>();

        private List<T> addBuffer = new List<T>();
        private List<T> removeBuffer = new List<T>();

        protected IReadOnlyList<T> AsReadOnlyList() => objects.Except(removeBuffer).Union(addBuffer).ToList().AsReadOnly();

        protected Container()
        {
            RegisterCollection(objects);
        }

        internal T AddObject(T obj)
        {
            if (objects.Contains(obj)) throw new global::System.Exception($"This {obj.GetType().Name} is already inside this {GetType().Name}");

            addBuffer.Add(obj);
            obj.Owner = this;
            return obj;
        }

        internal void RemoveObject(T obj)
        {
            if (!objects.Contains(obj)) throw new Exceptions.MissingObjectException<T>(obj, this);

            removeBuffer.Add(obj);
        }

        private void PreUpdate()
        {
            SyncAddBuffer();
            SyncRemoveBuffer();
        }

        private void SyncAddBuffer()
        {
            var addBufferBuffer = new List<T>(addBuffer);
            addBuffer.Clear();
            foreach (T obj in addBufferBuffer)
            {
                objects.Add(obj);
                MessageSystem.SendMessage(obj, "Start");
            }
        }

        private void SyncRemoveBuffer()
        {
            var removeBufferBuffer = new List<T>(removeBuffer);
            removeBuffer.Clear();
            foreach (T obj in removeBufferBuffer)
            {
                objects.Remove(obj);
                MessageSystem.SendMessage(obj, "Removed");
            }
        }
    }
}
