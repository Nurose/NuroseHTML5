﻿using Bridge.Html5;
using NuroseMobile.Graphics;
using NuroseMobile.System.Components;
using NuroseMobile.Types;
using System.Collections.Generic;

namespace NuroseMobile.System
{
    public sealed class Scene : Container<Entity>
    {
        public string Name { get; set; }
        public Game Game { get; }
        public Color BackgroundColor { get; set; } = new Color(235, 235, 235);
        public string BackgroundImage
        {
            get => _backgroundImage; set
            {
                backgroundImageElement.Src = value;
                _backgroundImage = value;
            }
        }
        private HTMLImageElement backgroundImageElement = new HTMLImageElement();
        private string _backgroundImage = "";

        public Vector2f Panning { get; set; }

        /// <summary>
        /// Please use <see cref="Game.CreateScene(string)"/> to create a Scene
        /// </summary>
        /// <param name="name"></param>
        /// <param name="game"></param>
        internal Scene(string name, Game game) : base()
        {
            Name = name;
        }

        public Entity AddEntity(Entity entity)
        {
            return AddObject(entity);
        }

        public void RemoveEntity(Entity entity)
        {
            RemoveObject(entity);
        }

        public ICollection<T> GetAllComponentsOfType<T>() where T : Component
        {
            List<T> found = new List<T>();
            foreach (Entity entity in AsReadOnlyList())
            {
                var f = entity.GetComponents<T>();
                if (f.Length > 0)
                    found.AddRange(f);
            }
            return found;
        }

        private void Render()
        {
            Renderer.Offset = Panning;
            Renderer.Clear(BackgroundColor);
            if (string.IsNullOrWhiteSpace(BackgroundImage)) return;
            Renderer.DrawImage(backgroundImageElement, Panning, Renderer.Resolution, depth: float.PositiveInfinity);
        }

        public override void Destroy()
        {
            throw new global::System.Exception("Can't destroy a scene");
        }
    }
}
