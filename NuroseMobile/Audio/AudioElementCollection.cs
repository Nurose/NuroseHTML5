﻿using Bridge.Html5;
using NuroseMobile.System;
using System.Collections.Generic;
using System.Linq;

namespace NuroseMobile.Audio
{
    public struct AudioElementCollection
    {
        public readonly string Source;
        private readonly List<HTMLAudioElement> elements;

        private bool looping;

        public AudioElementCollection(string source)
        {
            Source = source;
            elements = new List<HTMLAudioElement>();
            looping = false;
        }

        /// <summary>
        /// Plays the sound at a given volume ranged 0.0 - 1.0. Default 1
        /// </summary>
        /// <param name="volume"></param>
        public void Play(float volume = 1)
        {
            var elem = FindOrCreateFreeAudioElement();
            elem.CurrentTime = 0;
            elem.Volume = volume;
            elem.Play();
        }

        public void Stop()
        {
            foreach (HTMLAudioElement element in elements)
                element.Pause();
        }

        public void SetLoop(bool v)
        {
            foreach (HTMLAudioElement element in elements)
                element.Loop = v;

            looping = v;
        }

        public bool GetLoop()
        {
            return looping;
        }

        private HTMLAudioElement FindOrCreateFreeAudioElement()
        {
            HTMLAudioElement free = elements.FirstOrDefault(e => !e.Paused || e.Ended);
            if (free == null)
            {
                free = Document.CreateElement<HTMLAudioElement>("audio");
                free.Preload = "auto";
                free.Src = Source;
                free.AutoPlay = false;
                elements.Add(free);
            }
            return free;
        }
    }
}
