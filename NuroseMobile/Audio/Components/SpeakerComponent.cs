﻿using NuroseMobile.System.Components;

namespace NuroseMobile.Audio.Components
{
    public sealed class SpeakerComponent : Component
    {
        public string AudioFilePath
        {
            get => _audioFilePath;

            set
            {
                _audioFilePath = value;
                Sound = AudioPlayer.Get(_audioFilePath);
            }
        }
        public bool PlayOnStart { get; set; } = true;
        public bool Loop { get; set; }
        /// <summary>
        /// Volume ranged 0.0 - 1.0
        /// </summary>
        public float Volume { get; set; } = 1;

        private AudioElementCollection Sound;
        private string _audioFilePath;

        private void Start()
        {
            if (PlayOnStart)
                Play();
        }

        public void Play()
        {
            Sound.Play(Volume);
            Sound.SetLoop(Loop);
        }

        public void Stop()
        {
            Sound.Stop();
        }
    }
}
