﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuroseMobile.Audio
{
    [Obsolete]
    public sealed class Sound
    {
        internal AudioElementCollection AudioElements;

        public void Play()
        {

        }

        public void Stop()
        {

        }

        public bool Loop { get; set; }
    }
}
