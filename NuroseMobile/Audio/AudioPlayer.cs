﻿using System.Collections.Generic;
using System.Linq;

namespace NuroseMobile.Audio
{
    public static class AudioPlayer
    {
        private static readonly Dictionary<string, AudioElementCollection> sounds = new Dictionary<string, AudioElementCollection>();

        public static AudioElementCollection Get(string source)
        {
            AudioElementCollection result;
            if (sounds.TryGetValue(source, out result))
                return result;

            result = new AudioElementCollection(source);
            sounds.Add(source, result);
            return result;
        }
    }
}
